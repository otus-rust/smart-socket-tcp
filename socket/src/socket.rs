#[cfg(feature = "stp")]
use std::cell::RefCell;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
#[cfg(not(feature = "stp"))]
use std::io::Write;

#[derive(Debug, Clone, Default)]
pub struct SmartSocket {
    pub name: String,
    pub switch: bool,
    pub voltage: u32,
    pub amperage: u32,
    #[cfg(feature = "stp")]
    pub _iob: RefCell<String>,
}

impl Display for SmartSocket {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "Device: socket")?;
        writeln!(f, "└ Name: {}", self.name)?;
        writeln!(f, "└ State: {}", if self.switch { "on" } else { "off" })?;
        writeln!(f, "└ Voltage: {}", self.voltage)?;
        writeln!(
            f,
            "└ Amperage: {}",
            if self.switch { self.amperage } else { 0 }
        )?;
        writeln!(
            f,
            "└ Power: {}",
            if self.switch {
                self.voltage * self.amperage
            } else {
                0
            }
        )
    }
}

impl SmartSocket {
    pub fn new() -> Self {
        Self {
            name: "".into(),
            switch: false,
            voltage: 0,
            amperage: 0,
            #[cfg(feature = "stp")]
            _iob: RefCell::default(),
        }
    }
    pub fn with_name<N: AsRef<str>>(mut self, name: N) -> Self {
        self.name = name.as_ref().to_string();
        self
    }

    pub fn with_switch(mut self, switch: bool) -> Self {
        self.switch = switch;
        self
    }

    pub fn with_voltage(mut self, voltage: u32) -> Self {
        self.voltage = voltage;
        self
    }

    pub fn with_amperage(mut self, amperage: u32) -> Self {
        self.amperage = amperage;
        self
    }

    #[cfg(not(feature = "stp"))]
    pub fn input(&self) -> Result<String, Box<dyn Error>> {
        let mut buf = String::new();
        std::io::stdin().read_line(&mut buf)?;
        Ok(buf.trim().to_string())
    }

    #[cfg(feature = "stp")]
    pub fn input(&self) -> Result<String, Box<dyn Error>> {
        Ok(self._iob.replace(String::new()))
    }

    #[cfg(not(feature = "stp"))]
    pub fn output(&self, buf: String) -> Result<(), Box<dyn Error>> {
        std::io::stdout().write_fmt(format_args!("{buf}\n"))?;
        Ok(())
    }

    #[cfg(feature = "stp")]
    pub fn output<D: AsRef<str>>(&self, buf: D) -> Result<(), Box<dyn Error>> {
        self._iob.replace(buf.as_ref().to_string());
        Ok(())
    }

    pub fn show_main_menu(&self) -> Result<(), Box<dyn Error>> {
        self.output(["Main menu:", "1. Show info", "2. Change info", "_. Exit"].join("\n"))
    }

    pub fn show_change_menu(&self) -> Result<(), Box<dyn Error>> {
        self.output(
            [
                "Change menu:",
                "1. Update name",
                "2. Switch on/off",
                "3. Update voltage",
                "4. Update amperage",
                "_. Exit",
            ]
            .join("\n"),
        )
    }

    pub fn show_info(&self) -> Result<(), Box<dyn Error>> {
        self.output(self.to_string())
    }
}
