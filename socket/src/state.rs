use std::error::Error;
use std::fmt;

use derive_more::Display;

use crate::socket::SmartSocket;

pub type StateResult = Result<Box<dyn State + Send>, Box<dyn Error>>;

pub trait State: fmt::Display + fmt::Debug + Send {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult;

    #[cfg(feature = "stp")]
    fn is_action(&self) -> bool {
        false
    }

    fn is_exit(&self) -> bool {
        false
    }
}

#[derive(Display, Debug)]
pub struct ShowInfo;

impl State for ShowInfo {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        socket.show_info()?;
        Ok(Box::new(MainMenu))
    }
}

#[derive(Display, Debug)]
pub struct MainMenu;

impl State for MainMenu {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        socket.show_main_menu()?;
        Ok(Box::new(MainMenuAction))
    }
}

#[derive(Display, Debug)]
pub struct MainMenuAction;

impl State for MainMenuAction {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        match socket.input()?.as_str() {
            "1" => Ok(Box::new(ShowInfo)),
            "2" => Ok(Box::new(ChangeMenu)),
            _ => Ok(Box::new(Exit)),
        }
    }
    #[cfg(feature = "stp")]
    fn is_action(&self) -> bool {
        true
    }
}

#[derive(Display, Debug)]
pub struct ChangeMenu;

impl State for ChangeMenu {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        socket.show_change_menu()?;
        Ok(Box::new(ChangeMenuAction))
    }
}

#[derive(Display, Debug)]
pub struct ChangeMenuAction;

impl State for ChangeMenuAction {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        match socket.input()?.as_str() {
            "1" => Ok(Box::new(UpdateAction(Change::Name))),
            "2" => Ok(Box::new(UpdateAction(Change::Switch))),
            "3" => Ok(Box::new(UpdateAction(Change::Voltage))),
            "4" => Ok(Box::new(UpdateAction(Change::Amperage))),
            _ => Ok(Box::new(MainMenu)),
        }
    }
    #[cfg(feature = "stp")]
    fn is_action(&self) -> bool {
        true
    }
}

#[derive(Display, Debug, PartialEq, Eq)]
pub enum Change {
    Name,
    Switch,
    Voltage,
    Amperage,
}

#[derive(Display, Debug)]
pub struct UpdateAction(Change);

impl State for UpdateAction {
    fn update(&mut self, socket: &mut SmartSocket) -> StateResult {
        match self.0 {
            Change::Name => socket.name = socket.input()?,
            Change::Switch => socket.switch ^= true,
            Change::Voltage => socket.voltage = socket.input()?.parse::<u32>()?,
            Change::Amperage => socket.amperage = socket.input()?.parse::<u32>()?,
        }
        Ok(Box::new(ChangeMenu))
    }
    #[cfg(feature = "stp")]
    fn is_action(&self) -> bool {
        self.0 != Change::Switch
    }
}

#[derive(Display, Debug)]
pub struct Exit;

impl State for Exit {
    fn update(&mut self, _: &mut SmartSocket) -> StateResult {
        Ok(Box::new(Exit))
    }

    fn is_exit(&self) -> bool {
        true
    }
}
