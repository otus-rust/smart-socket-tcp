#[cfg(not(feature = "stp"))]
use socket::socket::SmartSocket;
#[cfg(not(feature = "stp"))]
use socket::state::*;
#[cfg(not(feature = "stp"))]
use std::error::Error;

#[cfg(feature = "stp")]
fn main() {
    eprintln!("Does not work with 'stp' feature!");
}

#[cfg(not(feature = "stp"))]
fn main() -> Result<(), Box<dyn Error>> {
    let mut socket = SmartSocket::new()
        .with_name("standard")
        .with_switch(true)
        .with_voltage(220)
        .with_amperage(15);

    let mut state: Box<dyn State> = Box::new(MainMenu);
    while !state.is_exit() {
        state = state.update(&mut socket)?;
    }

    println!("Exiting...");
    Ok(())
}
