use std::error::Error;

use socket::socket::SmartSocket;
use socket::state::{MainMenu, State};

pub struct RequestHandler {
    socket: SmartSocket,
    state: Box<dyn State + Send>,
}

impl RequestHandler {
    pub fn new(socket: SmartSocket) -> Self {
        Self {
            socket,
            state: Box::new(MainMenu),
        }
    }

    pub fn is_exit(&self) -> bool {
        self.state.is_exit()
    }

    pub fn is_action(&self) -> bool {
        self.state.is_action()
    }

    pub async fn handle(&mut self, request: String) -> Result<String, Box<dyn Error>> {
        let mut req = request;
        let mut buf = String::new();
        loop {
            self.socket.output(req.as_str())?;
            self.state = self.state.update(&mut self.socket)?;
            buf.push_str(self.socket.input()?.as_str());
            if self.is_exit() {
                buf.push_str("!!!exit");
                break;
            }
            if self.is_action() {
                break;
            }
            req = "".into();
        }
        Ok(buf)
    }
}
