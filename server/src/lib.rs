pub mod handler;

use chrono::Local;
use env_logger::{Builder, Env, WriteStyle};
use std::io::Write;

/// Logger initialization
pub fn log_init() {
    #[cfg(debug_assertions)]
    Builder::from_env(Env::default().default_filter_or("debug"))
        .write_style(WriteStyle::Auto)
        .format(|fmt, rec| {
            writeln!(
                fmt,
                "[{} {:5} {}:{}] {}",
                Local::now().format("%FT%X"),
                fmt.default_level_style(rec.level()).value(rec.level()),
                rec.file().unwrap_or("-"),
                rec.line().unwrap_or(0),
                rec.args()
            )
        })
        .init();
    #[cfg(not(debug_assertions))]
    Builder::from_env(Env::default().default_filter_or("info"))
        .write_style(WriteStyle::Auto)
        .format(|fmt, rec| {
            writeln!(
                fmt,
                "[{} {:5} {}] {}",
                Local::now().format("%FT%X"),
                fmt.default_level_style(rec.level()).value(rec.level()),
                rec.target(),
                rec.args()
            )
        })
        .init();
}
