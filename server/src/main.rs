use std::error::Error;
use std::fs;

use async_std::task;
use log::{error, info, warn};

use server::handler::RequestHandler;
use server::log_init;
use socket::socket::SmartSocket;
use stp::server::{StpConnection, StpServer};

#[async_std::main]
async fn main() {
    log_init();

    match run().await {
        Ok(_) => (),
        Err(e) => error!("{}", e),
    }
    warn!("Exiting...")
}

async fn run() -> Result<(), Box<dyn Error>> {
    let addr = fs::read_to_string("settings/addr").unwrap_or_else(|_| "127.0.0.1:6789".into());
    let server = StpServer::bind(addr.to_owned()).await?;

    let socket_default = SmartSocket::new()
        .with_name("standard")
        .with_switch(true)
        .with_voltage(220)
        .with_amperage(15);

    info!("Server started on {}", addr);

    loop {
        let connection = server.accept().await?;

        let addr = match connection.peer_addr() {
            Ok(addr) => addr.to_string(),
            Err(_) => "unknown".into(),
        };

        info!("New client connected: {}", addr);

        let socket = socket_default.clone();
        task::spawn(async move {
            match handle_connection(connection, socket).await {
                Ok(_) => info!("Client disconnected: {}", addr),
                Err(e) => error!("Client disconnected: {} with error: {}", addr, e),
            }
        });
    }
}

async fn handle_connection(
    mut conn: StpConnection,
    socket: SmartSocket,
) -> Result<(), Box<dyn Error>> {
    let mut handler = RequestHandler::new(socket);
    loop {
        let request = conn.recv_request().await?;
        let response = handler.handle(request).await?;
        conn.send_response(response).await?;
        if handler.is_exit() {
            conn.shutdown()?;
            break;
        }
    }
    Ok(())
}
