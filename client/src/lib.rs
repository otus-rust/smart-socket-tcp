use async_std::net::ToSocketAddrs;

use stp::client::StpClient;
use stp::error::{ConnectResult, RequestResult};

pub struct Client {
    stp: StpClient,
}

impl Client {
    pub async fn new<A: ToSocketAddrs>(addr: A) -> ConnectResult<Self> {
        let stp = StpClient::connect(addr).await?;
        Ok(Self { stp })
    }

    pub async fn send<R: AsRef<str>>(&mut self, request: R) -> RequestResult {
        self.stp.send_request(request).await
    }

    pub fn shutdown(&self) -> ConnectResult<()> {
        self.stp.shutdown()
    }
}
