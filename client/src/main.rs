use std::error::Error;
use std::{fs, io};

use client::Client;

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let addr = fs::read_to_string("settings/addr").unwrap_or_else(|_| "127.0.0.1:6789".to_string());
    let mut client = Client::new(addr).await?;

    let mut buf = String::new();

    loop {
        let request = buf.trim();
        let response = client.send(request).await?;
        let mut resp = response.split("!!!");
        println!("{}", resp.next().unwrap_or(""));
        if let Some("exit") = resp.next() {
            client.shutdown()?;
            break;
        }
        buf = String::new();
        io::stdin().read_line(&mut buf)?;
    }
    println!("Exiting...");
    Ok(())
}
