use std::io;
use std::net::{Shutdown, SocketAddr};

use async_std::io::prelude::*;
use async_std::net::{TcpListener, TcpStream, ToSocketAddrs};
use log::info;

use crate::error::{BindResult, ConnectError, ConnectResult, RecvResult, SendResult};

/// Represent STP server, that can accept incoming connections.
pub struct StpServer {
    tcp: TcpListener,
}

impl StpServer {
    /// Binds server to specified socket.
    pub async fn bind<A>(addrs: A) -> BindResult
    where
        A: ToSocketAddrs,
    {
        let tcp = TcpListener::bind(addrs).await?;
        Ok(Self { tcp })
    }

    /// Non-blocking accepting connections.
    pub async fn accept(&self) -> ConnectResult<StpConnection> {
        let (stream, addr) = self.tcp.accept().await?;
        info!("Accepting from: {}", addr);
        Self::try_handshake(stream).await
    }

    async fn try_handshake(mut stream: TcpStream) -> ConnectResult<StpConnection> {
        let mut buf = [0; 9];
        stream.read_exact(&mut buf).await?;
        if &buf != b"stpclient" {
            let msg = format!("received: {:?}", buf);
            return Err(ConnectError::BadHandshake(msg));
        }
        stream.write_all(b"stpserver").await?;
        Ok(StpConnection { stream })
    }
}

/// Represent connection from client.
///
/// Allows to receive requests and send responses.
pub struct StpConnection {
    stream: TcpStream,
}

impl StpConnection {
    /// Send response to client
    pub async fn send_response<R: AsRef<str>>(&mut self, response: R) -> SendResult {
        crate::send_string(&mut self.stream, response).await
    }

    /// Receive requests from client
    pub async fn recv_request(&mut self) -> RecvResult {
        crate::recv_string(&mut self.stream).await
    }

    /// Address of server
    pub fn local_addr(&self) -> io::Result<SocketAddr> {
        self.stream.local_addr()
    }

    /// Address of connected client
    pub fn peer_addr(&self) -> io::Result<SocketAddr> {
        self.stream.peer_addr()
    }

    /// Shutdown connection
    pub fn shutdown(&self) -> ConnectResult<()> {
        self.stream.shutdown(Shutdown::Both)?;
        Ok(())
    }
}
