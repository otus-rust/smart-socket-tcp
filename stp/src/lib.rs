use std::mem::size_of;

use async_std::io::prelude::*;

use crate::error::{RecvError, RecvResult, SendResult};

pub mod client;
pub mod error;
pub mod server;

async fn send_string<W: Write + Unpin, D: AsRef<str>>(mut writer: W, data: D) -> SendResult {
    let bytes = data.as_ref().as_bytes();
    let len = bytes.len() as u16;
    let len_bytes = len.to_be_bytes();
    writer.write_all(&len_bytes).await?;
    writer.write_all(bytes).await?;
    Ok(())
}

async fn recv_string<R: Read + Unpin>(mut reader: R) -> RecvResult {
    let mut buf = [0; size_of::<u16>()];
    reader.read_exact(&mut buf).await?;
    let len = u16::from_be_bytes(buf);
    let mut buf = vec![0; len as _];
    reader.read_exact(&mut buf).await?;
    String::from_utf8(buf).map_err(RecvError::BadEncoding)
}
