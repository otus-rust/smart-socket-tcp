use async_std::io::prelude::*;
use async_std::net::{Shutdown, TcpStream, ToSocketAddrs};

use crate::error::{ConnectError, ConnectResult, RequestResult};

/// Represent client-side connection for STP
pub struct StpClient {
    stream: TcpStream,
}

impl StpClient {
    /// Try to connect to specified address and perform handshake.
    pub async fn connect<Addrs>(addrs: Addrs) -> ConnectResult<Self>
    where
        Addrs: ToSocketAddrs,
    {
        let stream = TcpStream::connect(addrs).await?;
        Self::try_handshake(stream).await
    }

    /// Send request to connected STP server.
    pub async fn send_request<R: AsRef<str>>(&mut self, req: R) -> RequestResult {
        crate::send_string(&mut self.stream, req).await?;
        let response = crate::recv_string(&mut self.stream).await?;
        Ok(response)
    }

    /// Shutdown connection
    pub fn shutdown(&self) -> ConnectResult<()> {
        self.stream.shutdown(Shutdown::Both)?;
        Ok(())
    }

    async fn try_handshake(mut stream: TcpStream) -> ConnectResult<Self> {
        stream.write_all(b"stpclient").await?;
        let mut buf = [0; 9];
        stream.read_exact(&mut buf).await?;
        if &buf != b"stpserver" {
            let msg = format!("received: {buf:?}");
            return Err(ConnectError::BadHandshake(msg));
        }
        Ok(Self { stream })
    }
}
