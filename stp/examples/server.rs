use std::error::Error;
use std::net::SocketAddrV4;
use std::str::FromStr;

use async_std::task;
use env_logger::Env;
use log::{debug, info};

use stp::server::{StpConnection, StpServer};

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init_from_env(Env::default().default_filter_or("debug"));
    let addr = SocketAddrV4::from_str("127.0.0.1:6789").unwrap();
    let server = StpServer::bind(addr).await?;
    info!("Server accepting connections on {}", addr);
    loop {
        let connection = server.accept().await?;
        let connection_task = process_connection(connection);
        task::spawn(async { connection_task.await.unwrap() });
    }
}

async fn process_connection(mut conn: StpConnection) -> Result<(), Box<dyn Error>> {
    let request = conn.recv_request().await?;
    debug!(
        "Request: '{}' from: {}",
        &request,
        conn.peer_addr().unwrap()
    );
    assert_eq!(request, "Hello, server");
    conn.send_response("Hello, client").await?;
    Ok(())
}
