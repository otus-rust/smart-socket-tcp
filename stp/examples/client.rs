use std::error::Error;

use stp::client::StpClient;

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut client = StpClient::connect("127.0.0.1:6789").await?;
    let response = client.send_request("Hello, server").await?;
    dbg!(&response);
    assert_eq!(response, "Hello, client");
    Ok(())
}
